# Pokemon Availability

## Summary
Full-stack web development project to create a searchable webpage to compare the availability of different Pokémon across regions and games.

## Description
Using the data provided by a [Reddit post](https://www.reddit.com/r/pokemon/comments/a6oine/pokemon_availability/), we are creating a web page allowing players of main-series Pokemon games to reference the availability of Pokemon across different games. For example, a user of our site would be able to tell that they could obtain a Latias in the midgame of Alpha Sapphire, the lategame of Ultra Moon, and the postgame of Sapphire, Emerald, HeartGold, and White 2. They would also be able to get a list of all Pokemon available in Red, Diamond, or X.

IMPORTANT NOTE: The currently-used abbreviations for main-series Pokémon games make it difficult to parse files that use them. For example, "S" can mean Silver (GSC, GS, SC), Sapphire (RSE, RS, SE), or Sun (SM, SUS, SMUSUM), and two mean SoulSilver. To make parsing significantly easier, two-character codes have been devised for all of the games contained in the data as follows:
* Re/Be/Ye - Red/Blue/Yellow
* Go/Si/Cr - Gold/Silver/Crystal
* Ru/Sa/Em - Ruby/Sapphire/Emerald
* Co - Colosseum
* FR/LG - FireRed/LeafGreen
* XD - XD: Gale of Darkness
* Di/Pe/Pt - Diamond/Pearl/Platinum
* HG/SS - HeartGold/SoulSilver
* Bl/Wh - Black/White
* B2/W2 - Black 2/White 2
* Xx/Yy - X/Y
* OR/AS - Omega Ruby/Alpha Sapphire
* Su/Mo - Sun/Moon
* US/UM - Ultra Sun/Ultra Moon

## Process
The project consists of multiple stages. Stage 1 is to establish a database and parse the data into that database. This involves preparing the data in a CSV file such that each game is represented by a two-character code. The parsing software reads the data from the CSV file and stores it in an SQL database.

Stage 2 is to build the website. We will be using React for the front-end framework and PHP to move data between the front-end and the database.