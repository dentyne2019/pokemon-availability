/*
 * Author: Denton Wood
 * Project Name: Pokemon Availability
 * Date Created: 12/19/2018
 */

package data.tablerecords;

/**
 * Represents a record in the type table and an instance of one of the 18
 * Pokemon types.
 *
 * @version 1.0
 * @author Denton Wood
 */
public class PokemonTypeRecord extends PokemonObjectRecord {
    public PokemonTypeRecord(int typeId, String typeName) {
        super(typeId, typeName);
    }
}
