/*
 * Author: Denton Wood
 * Project Name: Pokemon Availability
 * Date Created: 12/18/2018
 */

package data.tablerecords;

/**
 * Represents a row in the family table and an instance of a Pokemon family
 * (i.e. a base Pokemon and its evolutions). For example, the Bulbasaur
 * family consists of Bulbasaur, Ivysaur, and Venusaur.
 */
public class PokemonFamilyRecord extends PokemonObjectRecord {
    public PokemonFamilyRecord(int id, String name) {
        super(id, name);
    }
}
