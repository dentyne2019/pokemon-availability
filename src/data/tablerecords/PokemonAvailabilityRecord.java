/*
 * Author: Denton Wood
 * Project Name: Pokemon Availability
 * Date Created: 12/19/2018
 */

package data.tablerecords;

import java.util.Objects;

/**
 * Represents a row in the availability record of the database. Contains a
 * record of a particular Pokemon family's availability for a particular game
 * in a particular time period.
 *
 * @version 1.0
 * @author Denton Wood
 */
public class PokemonAvailabilityRecord {
    private int pokemonFamilyId;
    private int gameId;
    private int timePeriodId;
    private String notes;

    public PokemonAvailabilityRecord(int pokemonFamilyId, int gameId,
                                     int timePeriodId, String notes) {
        this.pokemonFamilyId = pokemonFamilyId;
        this.gameId = gameId;
        this.timePeriodId = timePeriodId;
        this.notes = notes;
    }

    public int getPokemonFamilyId() {
        return this.pokemonFamilyId;
    }

    public void setPokemonFamilyId(int pokemonFamilyId) {
        this.pokemonFamilyId = pokemonFamilyId;
    }

    public int getGameId() {
        return this.gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getTimePeriodId() {
        return this.timePeriodId;
    }

    public void setTimePeriodId(int timePeriodId) {
        this.timePeriodId = timePeriodId;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PokemonAvailabilityRecord record = (PokemonAvailabilityRecord) o;
        return this.pokemonFamilyId == record.getPokemonFamilyId() &&
                this.gameId == record.getGameId() &&
                this.timePeriodId == record.getTimePeriodId() &&
                Objects.equals(this.notes, record.getNotes());
    }

    @Override
    public int hashCode() {
        return Objects.hash(pokemonFamilyId, gameId, timePeriodId, notes);
    }

    @Override
    public String toString() {
        return "Pokemon Availability: " + pokemonFamilyId + " | " + gameId +
                " | " + timePeriodId + " | " + notes;
    }
}
