/*
 * Author: Denton Wood
 * Project Name: Pokemon Availability
 * Date Created: 12/18/2018
 */

package data.tablerecords;

import java.util.Objects;

/**
 * Represents a row in the game table of the database and an instance of a
 * Pokemon game.
 *
 * @version 1.0
 * @author Denton Wood
 */
public class PokemonGameRecord extends PokemonObjectRecord {
    private String abbreviation;

    public PokemonGameRecord(int id, String name, String abbreviation) {
        super(id, name);
        this.abbreviation = abbreviation;
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }

    @Override
    public String toString() {
        return super.toString() + " (" + this.abbreviation + ")";
    }
}
