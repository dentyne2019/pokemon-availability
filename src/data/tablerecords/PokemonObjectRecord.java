package data.tablerecords;

/**
 * Generic class for a data record of a table representing an object.
 *
 * @version 1.0
 * @author Denton Wood
 */
public abstract class PokemonObjectRecord {
    private int id;
    private String name;

    public PokemonObjectRecord(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.id + ": " + this.name;
    }
}
