/*
 * Author: Denton Wood
 * Project Name: Pokemon Availability
 * Date Created: 12/19/2018
 */

package data;

import data.tablerecords.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles tablerecords specific to the availability project. Wraps an instance of
 * TableHandler.
 *
 * @version 1.0
 * @author Denton Wood
 */
public class PokemonAvailabilityTableHandler {
    /** The wrapped table handler instance */
    private TableHandler tableHandler;

    /**
     * Constructor for the class. Passes the parameters to create a database
     * connection to the table handler.
     *
     * @param dbUrl the database URL
     * @param dbUsername username to access the database
     * @param dbPassword password to access the database
     * @throws SQLException if there is an issue creating the connection
     */
    public PokemonAvailabilityTableHandler(String dbUrl, String dbUsername,
                                           String dbPassword)
            throws SQLException {
        this.tableHandler = new TableHandler(dbUrl, dbUsername, dbPassword);
    }

    /**
     * Loads the game table from the database.
     *
     * @return a list of games loaded from the database
     * @throws SQLException if the table cannot be loaded
     */
    public List<PokemonGameRecord> loadGames() throws SQLException {
        ResultSet results = tableHandler.loadTable("game");
        List<PokemonGameRecord> games = new ArrayList<>();
        if (results.next()) {
            do {
                int id = results.getInt(1);
                String name = results.getString(2);
                String abbreviation = results.getString(3);
                PokemonGameRecord game = new PokemonGameRecord(id, name, abbreviation);
                games.add(game);
            } while (results.next());
        }

        return games;
    }

    /**
     * Loads the pokemon_family table from the database
     *
     * @return a list of Pokemon families from the database
     * @throws SQLException if the table cannot be loaded
     */
    public List<PokemonFamilyRecord> loadFamilies() throws SQLException {
        ResultSet results = tableHandler.loadTable("pokemon_family");
        List<PokemonFamilyRecord> families = new ArrayList<>();
        if (results.next()) {
            do {
                int id = results.getInt(1);
                String name = results.getString(2);
                PokemonFamilyRecord family = new PokemonFamilyRecord(id, name);
                families.add(family);
            } while (results.next());
        }

        return families;
    }

    /**
     * Loads the time_obtained table from the database
     *
     * @return the times that Pokemon can be obtained from the database
     * @throws SQLException if the table cannot be loaded
     */
    public List<PokemonTimeRecord> loadTimes() throws SQLException {
        ResultSet results = tableHandler.loadTable("time_obtained");
        List<PokemonTimeRecord> times = new ArrayList<>();
        if (results.next()) {
            do {
                int id = results.getInt(1);
                String name = results.getString(2);
                PokemonTimeRecord time = new PokemonTimeRecord(id, name);
                times.add(time);
            } while (results.next());
        }

        return times;
    }

    /**
     * Loads the pokemon_type table from the database
     *
     * @return the types of Pokemon available from the table
     * @throws SQLException if the table cannot be loaded
     */
    public List<PokemonTypeRecord> loadTypes() throws SQLException {
        ResultSet results = tableHandler.loadTable("pokemon_type");
        List<PokemonTypeRecord> types = new ArrayList<>();
        if (results.next()) {
            do {
                int id = results.getInt(1);
                String name = results.getString(2);
                PokemonTypeRecord type = new PokemonTypeRecord(id, name);
                types.add(type);
            } while (results.next());
        }

        return types;
    }

    public void insertFamilies(List<PokemonFamilyRecord> families) {

    }

    public void insertAvailability(List<PokemonAvailabilityRecord> records) {

    }

    public void closeConnection() throws SQLException {
        this.tableHandler.closeConnection();
    }
}
