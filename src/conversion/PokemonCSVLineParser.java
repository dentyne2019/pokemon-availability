/*
 * Author: Denton Wood
 * Project Name: Pokemon Availability
 * Date Created: 12/18/2018
 */

package conversion;

import data.tablerecords.PokemonAvailabilityRecord;
import data.tablerecords.PokemonFamilyRecord;
import data.tablerecords.PokemonGameRecord;
import data.tablerecords.PokemonTimeRecord;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses a line in the given CSV file. This line will contain one Pokemon
 * and its availability across all main-series Pokemon games for 5 time
 * periods: early game, fair game, mid game, late game, and post game.
 *
 * @version 1.0
 * @author Denton Wood
 */
public class PokemonCSVLineParser {
    private List<PokemonGameRecord> games;
    private List<PokemonTimeRecord> times;
    private List<PokemonFamilyRecord> families;
    public static String CSV_LINE_PATTERN = "^(.+),(.*),(.*),(.*),(.*),(.*)$";
    public static String CSV_CELL_PATTERN = "([A-Za-z0-9]+)( (\\((.+?)\\)))?";
    public static int NUM_TIME_COLUMNS = 5;

    /**
     * Constructor for the class. Takes a list of PokemonGame records and
     * PokemonTime records to get IDs for the creation of PokemonAvailability
     * records.
     *
     * @param games list of PokemonGame records retrieved from database
     * @param times list of PokemonTime records retrieved from database
     */
    public PokemonCSVLineParser(List<PokemonGameRecord> games,
                                List<PokemonTimeRecord> times) {
        if (games == null || times == null) {
            throw new IllegalArgumentException("Arguments cannot be null");
        }
        this.games = games;
        this.times = times;
        this.families = new ArrayList<>();
    }

    /**
     * Parses a line consisting of multiple cells of a CSV file. Each line
     * contains the name of the Pokemon family in the first column and
     * contains five other columns, each representing a time period in the
     * game (early game, fair game, mid game, late game, and post game). Each
     * cell contains the games in which that Pokemon is available at that time.
     * The parsed results are stored in PokemonAvailability records to be stored
     * in the database. The PokemonFamily list contained by the class is also
     * updated so that it can be stored in the database.
     *
     * @param line the CSV line to be parsed
     * @return the parsed lines as availability records
     * @throws IOException if the line cannot be read
     */
    public List<PokemonAvailabilityRecord> parseLine(String line)
            throws IOException {
        Pattern p = Pattern.compile(CSV_LINE_PATTERN);
        Matcher m = p.matcher(line);
        if (!m.matches()) {
            throw new IOException("Invalid file format");
        }
        String pokemonFamilyName = m.group(1);
        int pokemonFamilyId = this.families.size() + 1;
        this.families.add(new PokemonFamilyRecord(pokemonFamilyId,
                pokemonFamilyName));
        List<PokemonAvailabilityRecord> records = new ArrayList<>();
        for (int i = 1; i <= NUM_TIME_COLUMNS; i++) {
            String cell = m.group(i + 1);
            List<PokemonAvailabilityRecord> cellRecords = parseCell(cell,
                    pokemonFamilyId, i);
            if (cellRecords != null) {
                records.addAll(cellRecords);
            }
        }
        return records;
    }

    /**
     * Parses a particular cell of a line of the CSV data input file. Each
     * cell is the cross of a particular Pokemon family (represented by the
     * family ID) and a particular time period in the game (represented by
     * the time ID). The cell contains two-character abbreviations that each
     * represent a particular game. A cell may also contain a note in
     * parentheses that applies to a series of games (each series is
     * space-delimited). The results are stored in PokemonAvailability records.
     *
     * @param cell the cell to parse
     * @param pokemonFamilyId the ID of the Pokemon family
     * @param timeId the ID of the time period
     * @return the parsed games as PokemonAvailability records
     * @throws IOException if the cell cannot be parsed
     */
    public List<PokemonAvailabilityRecord> parseCell(String cell,
                                                     int pokemonFamilyId,
                                                     int timeId)
            throws IOException {
        // Check arguments
        if (cell == null) {
            return null;
        }
        if (pokemonFamilyId <= 0 || timeId <= 0) {
            throw new IllegalArgumentException("IDs must be greater than 0");
        }

        Pattern p = Pattern.compile(CSV_CELL_PATTERN);
        Matcher m = p.matcher(cell);
        List<PokemonAvailabilityRecord> cellRecords = new ArrayList<>();
        while (m.find()) {
            // String of a space-delimited set of games - will be parsed below
            String gameString = m.group(1);
            // Any parenthetical notes attached to the set of games
            String notes = m.group(4);
            for (int i = 0; i < gameString.length(); i += 2) {
                if (gameString.length() < i + 2) {
                    throw new IOException("Invalid formatting of games");
                }
                String gameAbbreviation = gameString.substring(i, i + 2);
                int gameId = 0;
                for (PokemonGameRecord game: this.games) {
                    if (game.getAbbreviation().equals(gameAbbreviation)) {
                        gameId = game.getId();
                    }
                }
                if (gameId == 0) {
                    throw new IOException("Invalid game abbreviation received");
                }
                cellRecords.add(new PokemonAvailabilityRecord(pokemonFamilyId,
                        gameId, timeId, notes));
            }
        }

        return cellRecords;
    }

    /**
     * Getter for the list of PokemonGame records
     *
     * @return the list of game records
     */
    public List<PokemonGameRecord> getGames() {
        return List.copyOf(this.games);
    }

    /**
     * Getter for the list of PokemonTime records
     *
     * @return the list of time records
     */
    public List<PokemonTimeRecord> getTimes() {
        return List.copyOf(this.times);
    }

    /**
     * Getter for the list of PokemonFamily records
     *
     * @return the list of family records
     */
    public List<PokemonFamilyRecord> getFamilies() {
        return List.copyOf(this.families);
    }
}
