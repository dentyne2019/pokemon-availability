package conversion;

import data.tablerecords.PokemonAvailabilityRecord;
import data.tablerecords.PokemonGameRecord;
import data.tablerecords.PokemonTimeRecord;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PokemonCSVLineParserTest {
    public PokemonCSVLineParser parser;

    @BeforeEach
    public void initParser() {
        List<PokemonGameRecord> games = new ArrayList<>();
        games.add(new PokemonGameRecord(1, "Red", "Re"));
        games.add(new PokemonGameRecord(2, "Blue", "Be"));
        games.add(new PokemonGameRecord(3, "Yellow", "Ye"));
        games.add(new PokemonGameRecord(4, "Gold", "Go"));
        games.add(new PokemonGameRecord(5, "Silver", "Si"));
        games.add(new PokemonGameRecord(6, "Crystal", "Cr"));
        games.add(new PokemonGameRecord(7, "Ruby", "Ru"));
        games.add(new PokemonGameRecord(8, "Sapphire", "Sa"));
        games.add(new PokemonGameRecord(9, "Colosseum", "Co"));
        games.add(new PokemonGameRecord(10, "FireRed", "FR"));
        games.add(new PokemonGameRecord(11, "LeafGreen", "LG"));
        games.add(new PokemonGameRecord(12, "Emerald", "Em"));
        games.add(new PokemonGameRecord(13, "XD: Gale of Darkness", "XD"));
        games.add(new PokemonGameRecord(14, "Diamond", "Di"));
        games.add(new PokemonGameRecord(15, "Pearl", "Pe"));
        games.add(new PokemonGameRecord(16, "Platinum", "Pt"));
        games.add(new PokemonGameRecord(17, "HeartGold", "HG"));
        games.add(new PokemonGameRecord(18, "SoulSilver", "SS"));
        games.add(new PokemonGameRecord(19, "Black", "Bk"));
        games.add(new PokemonGameRecord(20, "White", "Wh"));
        games.add(new PokemonGameRecord(21, "Black 2", "B2"));
        games.add(new PokemonGameRecord(22, "White 2", "W2"));
        games.add(new PokemonGameRecord(23, "X", "Xx"));
        games.add(new PokemonGameRecord(24, "Y", "Yy"));
        games.add(new PokemonGameRecord(25, "Omega Ruby", "OR"));
        games.add(new PokemonGameRecord(26, "Alpha Sapphire", "AS"));
        games.add(new PokemonGameRecord(27, "Sun", "Su"));
        games.add(new PokemonGameRecord(28, "Moon", "Mo"));
        games.add(new PokemonGameRecord(29, "Ultra Sun", "US"));
        games.add(new PokemonGameRecord(30, "Ultra Moon", "UM"));

        List<PokemonTimeRecord> times = new ArrayList<>();
        times.add(new PokemonTimeRecord(1, "Earlygame"));
        times.add(new PokemonTimeRecord(2, "Fairgame"));
        times.add(new PokemonTimeRecord(3, "Midgame"));
        times.add(new PokemonTimeRecord(4, "Lategame"));
        times.add(new PokemonTimeRecord(5, "Postgame"));
        this.parser = new PokemonCSVLineParser(games, times);
    }

    @Nested
    @DisplayName("Constructor")
    public class ConstructorTest {
        @Test
        @DisplayName("Valid arguments")
        public void testValidArguments() {
            List<PokemonGameRecord> games = new ArrayList<>();
            List<PokemonTimeRecord> times = new ArrayList<>();
            PokemonCSVLineParser parser = new PokemonCSVLineParser(games,
                    times);
            assertEquals(games, parser.getGames());
            assertNotSame(games, parser.getGames());
            assertEquals(times, parser.getTimes());
            assertNotSame(times, parser.getTimes());
        }

        @Nested
        @DisplayName("Null arguments")
        public class NullTest {
            @Test
            @DisplayName("Game records")
            public void testGameRecords() {
                assertThrows(IllegalArgumentException.class,
                        () -> new PokemonCSVLineParser(null,
                                new ArrayList<>()));
            }

            @Test
            @DisplayName("Time records")
            public void testTimeRecords() {
                assertThrows(IllegalArgumentException.class,
                        () -> new PokemonCSVLineParser(new ArrayList<>(),
                                null));
            }
        }
    }

    @Nested
    @DisplayName("Cell Parse")
    public class CellParseTest {
        @Nested
        @DisplayName("Invalid values")
        public class InvalidValuesTest {
            @Test
            @DisplayName("Null")
            public void testNull() throws IOException {
                List<PokemonAvailabilityRecord> records = parser.parseCell(null,
                        1, 1);
                assertNull(records);
            }

            @ParameterizedTest(name = "familyID = {0}")
            @ValueSource(ints = {0, -1, Integer.MIN_VALUE})
            @DisplayName("Invalid family ID")
            public void testInvalidFamilyId(int id) throws IOException {
                assertThrows(IllegalArgumentException.class,
                        () -> parser.parseCell("HG", id, 1));
            }

            @ParameterizedTest(name = "timeId = {0}")
            @ValueSource(ints = {0, -1, Integer.MIN_VALUE})
            @DisplayName("Invalid time ID")
            public void testInvalidTimeId(int id) throws IOException {
                assertThrows(IllegalArgumentException.class,
                        () -> parser.parseCell("HG", 1, id));
            }
        }

        @Test
        @DisplayName("Empty")
        public void testEmpty() throws IOException {
            List<PokemonAvailabilityRecord> records =
                    parser.parseCell("", 1, 1);
            assertEquals(0, records.size());
        }

        @ParameterizedTest(name = "Input = {0}")
        @ValueSource(strings = {"a","bcd","Abc","AbCdEEf"})
        @DisplayName("Wrong abbreviation length")
        public void testWrongAbbreviationLength(String cell) {
            assertThrows(IOException.class, () -> parser.parseCell(cell, 1, 1));
        }

        @ParameterizedTest(name = "Input = {0}")
        @ValueSource(strings = {"Ab","123","HS","DPPt","GoSC"})
        @DisplayName("Invalid abbreviations")
        public void testInvalidAbbreviations(String cell) {
            assertThrows(IOException.class, () -> parser.parseCell(cell, 1, 1));
        }

        @Test
        @DisplayName("One Game")
        public void testOneGame() throws IOException {
            List<PokemonAvailabilityRecord> records = parser.parseCell("HG",
                    1, 1);
            List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
            expectedRecords.add(new PokemonAvailabilityRecord(1, 17, 1, null));
            assertEquals(expectedRecords, records);
        }

        @Test
        @DisplayName("Five Games")
        public void testFiveGames() throws IOException {
            List<PokemonAvailabilityRecord> records =
                    parser.parseCell("ReBeYeGoSi", 1, 1);
            List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                expectedRecords.add(new PokemonAvailabilityRecord(1, i + 1, 1,
                        null));
            }
            assertEquals(expectedRecords, records);
        }

        @Test
        @DisplayName("One Game with Note")
        public void testOneGameWithNote() throws IOException {
            List<PokemonAvailabilityRecord> records =
                    parser.parseCell("B2 (PokePelago)", 2, 3);
            List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
            expectedRecords.add(new PokemonAvailabilityRecord(2, 21, 3,
                    "PokePelago"));
            assertEquals(expectedRecords, records);
        }

        @Test
        @DisplayName("Five Games with One Note")
        public void testFiveGamesWithNote() throws IOException {
            List<PokemonAvailabilityRecord> records =
                    parser.parseCell("DiPePtHGSS (Scan)", 5, 6);
            List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                expectedRecords.add(new PokemonAvailabilityRecord(5, i + 14,
                        6, "Scan"));
            }
            assertEquals(expectedRecords, records);
        }

        @Test
        @DisplayName("Space-Separated Without Notes")
        public void testSpaceSeparatedWithoutNotes() throws IOException {
            List<PokemonAvailabilityRecord> records =
                    parser.parseCell("GoSiCr RuSaCo", 100, 200);
            List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                expectedRecords.add(new PokemonAvailabilityRecord(100, i + 4,
                        200, null));
            }
            for (int i = 0; i < 3; i++) {
                expectedRecords.add(new PokemonAvailabilityRecord(100, i + 7,
                        200, null));
            }
            assertEquals(expectedRecords, records);
        }

        @Test
        @DisplayName("Eight Games, Two Notes")
        public void testEightGamesTwoNotes() throws IOException {
            List<PokemonAvailabilityRecord> records =
                    parser.parseCell("BkWhB2W2 " +
                    "(Excl.) SuMoUSUM (Dongle)", 125, 225);
            List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                expectedRecords.add(new PokemonAvailabilityRecord(125, i + 19,
                        225, "Excl."));
            }
            for (int i = 0; i < 4; i++) {
                expectedRecords.add(new PokemonAvailabilityRecord(125, i + 27,
                        225, "Dongle"));
            }
            assertEquals(expectedRecords, records);
        }

        @Test
        @DisplayName("Difficult String")
        public void testDifficultString() throws IOException {
            List<PokemonAvailabilityRecord> records =
                    parser.parseCell("ReBeFRLG Ye (Starter) ORAS (Cosplay) " +
                            "XxYy SuMoUSUM (A)", 1024, 2048);
            List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
            expectedRecords.add(new PokemonAvailabilityRecord(1024, 1, 2048,
                    null));
            expectedRecords.add(new PokemonAvailabilityRecord(1024, 2, 2048,
                    null));
            expectedRecords.add(new PokemonAvailabilityRecord(1024, 10, 2048,
                    null));
            expectedRecords.add(new PokemonAvailabilityRecord(1024, 11, 2048,
                    null));
            expectedRecords.add(new PokemonAvailabilityRecord(1024, 3, 2048,
                    "Starter"));
            expectedRecords.add(new PokemonAvailabilityRecord(1024, 25, 2048,
                    "Cosplay"));
            expectedRecords.add(new PokemonAvailabilityRecord(1024, 26, 2048,
                    "Cosplay"));
            expectedRecords.add(new PokemonAvailabilityRecord(1024, 23, 2048,
                    null));
            expectedRecords.add(new PokemonAvailabilityRecord(1024, 24, 2048,
                    null));
            for (int i = 0; i < 4; i++) {
                expectedRecords.add(new PokemonAvailabilityRecord(1024, i + 27,
                        2048, "A"));
            }
            assertEquals(expectedRecords, records);
        }
    }

    @Nested
    @DisplayName("Row Parse")
    public class RowParserTest {
        @ParameterizedTest(name = "Row = {0}")
        @ValueSource(strings = {"", "a,b,c", "1,2,3,4", "abc123"})
        @DisplayName("Invalid row format")
        public void testInvalidRowFormat(String line) {
            assertThrows(IOException.class, () -> parser.parseLine(line));
        }

        @Test
        @DisplayName("One game per cell")
        public void testOneGamePerCell() throws IOException {
            List<PokemonAvailabilityRecord> records =
                    parser.parseLine("Bulbasaur,Re,Be,Ye,Go,Si");
            List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                expectedRecords.add(new PokemonAvailabilityRecord(1, i + 1,
                        i + 1, null));
            }
            assertEquals(expectedRecords, records);
        }

        @Nested
        @DisplayName("Cells without values")
        public class CellsWithoutValuesTest {
            @Test
            @DisplayName("Pokemon Family")
            public void testFamily() {
                assertThrows(IOException.class, () -> parser.parseLine(",Su," +
                        "Mo,US,UM,Re"));
            }

            @Test
            @DisplayName("First Cell")
            public void testFirstCell() throws IOException {
                List<PokemonAvailabilityRecord> records =
                        parser.parseLine("Caterpie,,Be,Ye,Go,Si");
                List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
                for (int i = 0; i < 5; i++) {
                    if (i + 1 != 1) {
                        expectedRecords.add(new PokemonAvailabilityRecord(1,
                                i + 1, i + 1, null));
                    }
                }
                assertEquals(expectedRecords, records);
            }

            @Test
            @DisplayName("Second Cell")
            public void testSecondCell() throws IOException {
                List<PokemonAvailabilityRecord> records =
                        parser.parseLine("Caterpie,Re,,Ye,Go,Si");
                List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
                for (int i = 0; i < 5; i++) {
                    if (i + 1 != 2) {
                        expectedRecords.add(new PokemonAvailabilityRecord(1,
                                i + 1, i + 1, null));
                    }
                }
                assertEquals(expectedRecords, records);
            }

            @Test
            @DisplayName("Third Cell")
            public void testThirdCell() throws IOException {
                List<PokemonAvailabilityRecord> records =
                        parser.parseLine("Caterpie,Re,Be,,Go,Si");
                List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
                for (int i = 0; i < 5; i++) {
                    if (i + 1 != 3) {
                        expectedRecords.add(new PokemonAvailabilityRecord(1,
                                i + 1, i + 1, null));
                    }
                }
                assertEquals(expectedRecords, records);
            }

            @Test
            @DisplayName("Fourth Cell")
            public void testFourthCell() throws IOException {
                List<PokemonAvailabilityRecord> records =
                        parser.parseLine("Caterpie,Re,Be,Ye,,Si");
                List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
                for (int i = 0; i < 5; i++) {
                    if (i + 1 != 4) {
                        expectedRecords.add(new PokemonAvailabilityRecord(1,
                                i + 1, i + 1, null));
                    }
                }
                assertEquals(expectedRecords, records);
            }

            @Test
            @DisplayName("Fifth Cell")
            public void testFifthCell() throws IOException {
                List<PokemonAvailabilityRecord> records =
                        parser.parseLine("Caterpie,Re,Be,Ye,Go,");
                List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
                for (int i = 0; i < 5; i++) {
                    if (i + 1 != 5) {
                        expectedRecords.add(new PokemonAvailabilityRecord(1,
                                i + 1, i + 1, null));
                    }
                }
                assertEquals(expectedRecords, records);
            }
        }

        @Test
        @DisplayName("Two games per cell")
        public void testTwoGamesPerCell() throws IOException {
            List<PokemonAvailabilityRecord> records =
                    parser.parseLine("Charmander,ReBe,YeGo,SiCr,RuSa,CoFR");
            List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 2; j++) {
                    // Increase from 1 to 10
                    int gameId = (i + 1) * 2 - (1 - j);
                    expectedRecords.add(new PokemonAvailabilityRecord(1, gameId,
                            i + 1, null));
                }
            }
            assertEquals(expectedRecords, records);
        }

        @Test
        @DisplayName("Multiple rows")
        public void testMultipleRows() throws IOException {
            List<PokemonAvailabilityRecord> records1 =
                    parser.parseLine("Turtwig,Di,Pe,Pt,HG,SS");
            List<PokemonAvailabilityRecord> records2 =
                    parser.parseLine("Piplub,Di,Pe,Pt,HG,SS");
            List<PokemonAvailabilityRecord> records3 =
                    parser.parseLine("Chmichar,Di,Pe,Pt,HG,SS");
            List<PokemonAvailabilityRecord> expectedRecords1 =
                    new ArrayList<>();
            List<PokemonAvailabilityRecord> expectedRecords2 =
                    new ArrayList<>();
            List<PokemonAvailabilityRecord> expectedRecords3 =
                    new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                expectedRecords1.add(new PokemonAvailabilityRecord(1, i + 14,
                        i + 1, null));
                expectedRecords2.add(new PokemonAvailabilityRecord(2, i + 14,
                        i + 1, null));
                expectedRecords3.add(new PokemonAvailabilityRecord(3, i + 14,
                        i + 1, null));
            }
            assertEquals(expectedRecords1, records1);
            assertEquals(expectedRecords2, records2);
            assertEquals(expectedRecords3, records3);
        }

        @Test
        @DisplayName("Notes Added")
        public void testNotesAdded() throws IOException {
            List<PokemonAvailabilityRecord> records =
                    parser.parseLine("Mankey,ReBe (Excl.),YeGo,SiCr " +
                            "(PokePelago),RuSa,CoFR (A)");
            List<PokemonAvailabilityRecord> expectedRecords = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 2; j++) {
                    // Increase from 1 to 10
                    int gameId = (i + 1) * 2 - (1 - j);
                    String note = null;
                    if (i == 0) {
                        note = "Excl.";
                    } else if (i == 2) {
                        note = "PokePelago";
                    } else if (i == 4) {
                        note = "A";
                    }
                    expectedRecords.add(new PokemonAvailabilityRecord(1, gameId,
                            i + 1, note));
                }
            }
            assertEquals(expectedRecords, records);
        }
    }
}
