CREATE DATABASE IF NOT EXISTS availability;
USE availability;

DROP TABLE IF EXISTS pokemon;
CREATE TABLE  pokemon (
	id INT PRIMARY KEY,
    species_name VARCHAR(100) NOT NULL,
    avatar LONGBLOB DEFAULT NULL,
    family_id INT NOT NULL,
    type1_id INT NOT NULL,
    type2_id INT DEFAULT NULL
);

DROP TABLE IF EXISTS pokemon_family;
CREATE TABLE pokemon_family (
	id INT PRIMARY KEY AUTO_INCREMENT,
    family_name VARCHAR(100) NOT NULL
);

DROP TABLE IF EXISTS pokemon_type;
CREATE TABLE pokemon_type (
	id INT PRIMARY KEY AUTO_INCREMENT,
    type_name VARCHAR(100) NOT NULL
);
INSERT INTO pokemon_type (type_name) VALUES
('Normal'),
('Fighting'),
('Flying'),
('Poison'),
('Ground'),
('Rock'),
('Bug'),
('Ghost'),
('Steel'),
('Fire'),
('Water'),
('Grass'),
('Electric'),
('Psychic'),
('Ice'),
('Dragon'),
('Dark'),
('Fairy');

DROP TABLE IF EXISTS time_obtained;
CREATE TABLE time_obtained (
	id INT PRIMARY KEY AUTO_INCREMENT,
    time_name VARCHAR(100) NOT NULL
);
INSERT INTO time_obtained (time_name) VALUES
('Earlygame'),
('Fairgame'),
('Midgame'),
('Lategame'),
('Postgame');

DROP TABLE IF EXISTS game;
CREATE TABLE game (
	id INT PRIMARY KEY AUTO_INCREMENT,
    game_name VARCHAR(100) NOT NULL,
    abbreviation VARCHAR(2) NOT NULL
);
INSERT INTO game (game_name, abbreviation) VALUES
('Red','Re'),
('Blue','Be'),
('Yellow','Ye'),
('Gold','Go'),
('Silver','Si'),
('Crystal','Cr'),
('Ruby','Ru'),
('Sapphire','Sa'),
('Colosseum','Co'),
('FireRed','FR'),
('LeafGreen','LG'),
('Emerald','Em'),
('XD: Gale of Darkness','XD'),
('Diamond','Di'),
('Pearl','Pe'),
('Platinum','Pt'),
('HeartGold','HG'),
('SoulSilver','SS'),
('Black','Bl'),
('White','Wh'),
('Black 2','B2'),
('White 2','W2'),
('X','Xx'),
('Y','Yy'),
('Omega Ruby','OR'),
('Alpha Sapphire','AS'),
('Sun','Su'),
('Moon','Mo'),
('Ultra Sun','US'),
('Ultra Moon','UM');

DROP TABLE IF EXISTS pokemon_availability;
CREATE TABLE pokemon_availability (
    pokemon_family_id INT NOT NULL,
    game_id INT NOT NULL,
    time_id INT NOT NULL,
    notes VARCHAR(100),
    PRIMARY KEY (pokemon_family_id, game_id, time_id)
);

DROP TABLE IF EXISTS item;
CREATE TABLE item (
	id INT PRIMARY KEY AUTO_INCREMENT,
    item_name VARCHAR(100) NOT NULL
);

DROP TABLE IF EXISTS evolves;
CREATE TABLE evolves (
	item_id INT NOT NULL,
    pokemon_id INT NOT NULL,
    PRIMARY KEY (item_id, pokemon_id)
);

DROP TABLE IF EXISTS item_availability;
CREATE TABLE item_availability (
	item_id INT NOT NULL,
    game_id INT NOT NULL,
    time_id INT NOT NULL,
    notes VARCHAR(100),
    PRIMARY KEY (item_id, game_id, time_id)
);